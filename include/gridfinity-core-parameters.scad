/*
* GRIDFINITY CORE PARAMETERS - Changing may break compatability.
* 
* If you edit these, the design may no longer be compatible with other Gridfinity models. 
*/

/* [Hidden] */

// Angle of Top Wedge from horizontal - We assume this to be 45 degrees as it avoids a lot of pointless maths clutter for virtually no gain
//GF_Top_Wedge_Angle = 45;
// Angle of Bottom Wedge from horizontal
GF_Bottom_Wedge_Angle = 45;
GF_Top_Wedge_Width = 2.15;
GF_Top_To_Bottom_Wedge_Distance = 1.8;
GF_Bottom_Wedge_Height = 0.7;
GF_Height_U = 7;
GF_U_Width = 42;
GF_Bin_Stacking_Lip_Top_Wedge_Width = 1.9;
// Tolerance fit for bin base to grid
GF_Bin_Socket_Clearance = 0.25;
GF_Bin_Bottom_Wedge_Width = 0.8;
// How much inset bins are on each edge from the edge of the base grid. Clearance to prevent bins jamming against each other
GF_Bin_Width_Side_Clearance = 0.25;
// Vertical side wall thickness
GF_Bin_Side_Wall_Thickness = 1.2;
// Lower wedge thickness: Fig 2: 0-1 X, limitation point 1 X <= GF_Corner_Radius due to OpenScad limitation for extrude_rotate()
GF_Bin_Lower_Wedge_Minimum_Thickness = 1.2;
// This is the corner radius for all parts of the bin and base profile. This is an offset of the profile vector relative to origin
GF_Corner_Radius = 4;
//
// Optional Mounting base
GF_Base_Mounting_Total_Thickness = 3.9;
GF_Base_Hole_Centre_From_Corner = 8;
GF_Base_Screw_Hole_Diameter = 3;
GF_Base_Magnet_Diameter = 6.5;
GF_Base_Magnet_Hole_Depth = 2.4;
GF_Base_Mounting_Base_Magnet_Pad = 2;

//*********************************************************************
//* Base Grid Diagram
//* 
//* Grid Base Profile - Fig 1
//*  6  __ 5
//*    |  \
//*    |   \
//*    |    | 4
//*    |    | 3
//*    |     \
//*    |      | 2
//*  0  ------  1
//*
//* Bin Base Profile - Fig 2
//*  
//*  5  _  4
//*    | |
//*    | |
//*  6 | | 3
//*    \ \
//*     \ \
//*   7  | \  2
//*      |  |  
//*   8   \ |
//*   0    -- 1
//*
//* Bin Lip Profile Standard - Fig 3
//*
//* Y:2 to align to top of bin wall (usually a U height boundary) + GF_Stacking_Compensation - so Y:5 + GF_Stacking_Compensation will be the Y origin
//*
//*  6  __ 5
//*    |  \
//*    |   \      45 deg
//*    |    | 4
//*    |    | 3
//*    |     \    45 deg
//*    |      | 2
//*    |      | 1
//*    |     /
//*    |   /      45 deg
//*  0 | /
//*
//* Bin Lip Profile Lite - Fig 4
//*
//* X:0-1 to be same as Fig 2: X:4-5
//* (Y:3 - GF_Top_To_Bottom_Wedge_Distance - GF_Bottom_Wedge_Height) to align to top of bin wall (usually a U height boundary) + GF_Stacking_Compensation
//*
//*  4  __ 3
//*    |  \
//*    |   \
//*    |    | 2
//*    |    | 1
//*    |   /
//*    |  /
//*  0 | / 

//*********************************************************************


/* [DOVETAIL PARAMETERS - these work - tweak at your own risk] */
// All round clearance so dovetail tab is a good fit in socket - This is one you may need to teak for your printer
DT_Clearance = 0.05;
// Dovetail long edge length
DT_Length = 4;
// Dovetail long edge to short edge distance
DT_Depth = 1.6;
// Dovetail tab thickness
DT_Thickness = 2;
// Dovetail angle
DT_Angle = 70;
// A little extra on the dovertail socket to ensure the tab seats fully
DT_Socket_Top_Clearance = 0.1;
/**********************************************************************/

