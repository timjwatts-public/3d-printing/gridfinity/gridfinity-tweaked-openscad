//**********************************************************************
//* Standard Core Library
//*
//* Things that you might expect the core language to have
//**********************************************************************

//**********************************************************************
//* Replicate the children in the x,y, z direction
//* x_count,y_count,z_count times
//* assuming offsets ebwteen copies of dx,dy,dz
//* 
//* Defaults allow simplified calling, such as only specifying x_count and dx
//* 
module s_linear_array(x_count=1, y_count=1, z_count=1, dx=0, dy=0, dz=0)
{
  for (z = [0:z_count - 1])
  {
    for (y = [0:y_count - 1])
    {
      for (x = [0:x_count - 1])
      {
        // Build extrusion and shift back so [0,0] maps to the lower left corner of the sketch
        translate([ x * dx, y * dy, z * dz ]) children();
      }
    }
  }
}
//**********************************************************************

//**********************************************************************
//* Rounded shape generator
//**********************************************************************

//**********************************************************************
//* Rounded Cuboid - edges in z direction are rounded , all others are square
module s_cube(size, corner_radius=0, center=false)
{
  if (corner_radius==0)
  {
    cube(size=size, center=center);
  }
  else
  {
    translate (center ? -size/2 : [0,0,0])
      hull()
      {
        translate([ corner_radius, corner_radius, 0 ]) cylinder(h = size[2], r = corner_radius);
        translate([ size[0] - corner_radius, corner_radius, 0 ]) cylinder(h = size[2], r = corner_radius);
        translate([ corner_radius, size[1] - corner_radius, 0 ]) cylinder(h = size[2], r = corner_radius);
        translate([ size[0] - corner_radius, size[1] - corner_radius, 0 ]) cylinder(h = size[2], r = corner_radius);
      }
  }
}

//**********************************************************************
//* Commom mirror operations
//**********************************************************************

//**********************************************************************
//* Assemble an object from 4 copies of a quadrant part    
module s_mirror_quad()    
    union()
    {
      children();
      mirror([ 1, 0, 0 ]) children();
      mirror([ 0, 1, 0 ]) children();
      mirror([ 0, 1, 0 ]) mirror([ 1, 0, 0 ]) children();
    }
//**********************************************************************

//**********************************************************************
//* X and Y distances between pairs of points in a polygon 
// aka (polygon[j] - polygon[j])[0]
function dx_polygon_std(polygon, i, j) = (polygon[j] - polygon[i])[0];
// aka (polygon[j] - polygon[j])[0]
function dy_polygon_std(polygon, i, j) = (polygon[j] - polygon[i])[1];
//**********************************************************************

//**********************************************************************
//* Extruder: Harde edged rectangle
// Generate lower left quadrant (-ve X, -ve Y) with origin shifted ready for reflective assembly
module _extrude_rectangle_quadrant(width, depth, profile)
{
  // Place in lower left quadrant
  translate([-width/2,-depth/2,0])
    // Make lower left quadrant of the shape
    union()
    {
      // 1/2 Left side
      translate ([0,-EPSILON,0]) mirror([0,1,0]) rotate(90, [1,0,0]) linear_extrude(height = depth/2 + 2*EPSILON) polygon(profile);
      //1/2 Bottom side
      translate ([-EPSILON,0,0]) rotate (90, [0,0,1]) rotate(90, [1,0,0]) linear_extrude(height = width/2 + 2*EPSILON) polygon(profile );
    }
}

// Assemble 4 of the above together using reflection
// Result will be a model centred on the origin
module s_extrude_rectangle(width, depth, profile, center=false)
{
  translate( center ? [0,0,0] : [width/2, depth/2, 0])
    s_mirror_quad() _extrude_rectangle_quadrant(width, depth, profile);

}
//**********************************************************************

//**********************************************************************
//* Extruder: Rounded rectangle
// Generate lower left quadrant (-ve X, -ve Y) with origin shifted ready for reflective assembly
module _extrude_rectangle_rounded_quadrant(width, depth, corner_radius, profile)
{
  // Place in lower left quadrant
  translate([-width/2,-depth/2,0])
    // Make lower left quadrant of the shape
    union()
    {
      // 1/2 Left side
      translate ([0,corner_radius-EPSILON,0]) mirror([0,1,0]) rotate(90, [1,0,0]) linear_extrude(height = (depth - 2*corner_radius)/2 + 2*EPSILON) polygon(profile);
      //1/2 Bottom side
      translate ([corner_radius-EPSILON,0,0]) rotate (90, [0,0,1]) rotate(90, [1,0,0]) linear_extrude(height = ( width - 2*corner_radius)/2 + 2*EPSILON) polygon(profile );
      // Bottom left corner
      translate([corner_radius,corner_radius,0]) rotate_extrude(angle=90) translate([-corner_radius,0,0]) polygon(profile);
    }
}

// Assemble 4 of the above together using reflection
// Result will be a model centred on the origin
module s_extrude_rectangle_rounded(width, depth, corner_radius, profile, center=false)
{
  translate( center ? [0,0,0] : [width/2, depth/2, 0])
    s_mirror_quad() _extrude_rectangle_rounded_quadrant(width, depth, corner_radius, profile);

}
//**********************************************************************
