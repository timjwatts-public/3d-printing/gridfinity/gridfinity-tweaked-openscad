//**********************************************************************
//* Core Base Modules and Functions

include <stdcore.scad>

//* Dependencies on Global Variables:
//* Stardard variables from gridfinity-core-parameters.scad
//*
//* GF_Top_Wedge_Width
//* GF_Bottom_Wedge_Width
//* GF_Bottom_Wedge_Height
//* GF_Top_To_Bottom_Wedge_Distance
//* GF_Base_Padding
//* GF_U_Width
//* GF_Corner_Radius

//**********************************************************************
//* Derived Parameters
GF_Bottom_Wedge_Width = GF_Bottom_Wedge_Height * tan(90 - GF_Bottom_Wedge_Angle);
GF_Top_Wedge_Height = (GF_Top_Wedge_Width - GF_Top_Shoulder_Width); // Assume angles are 45 degrees
GF_Base_Mounting_Width = GF_Base_Hole_Centre_From_Corner + GF_Base_Magnet_Diameter/2 + GF_Base_Mounting_Base_Magnet_Pad;

M_Bin_Floor_Thickness = 
  M_Floor_Type == "F" ? 
    GF_Height_U :
    M_Floor_Type == "T" ? bin_flat_floor_min_y() : M_Desired_Floor_Thickness;

// Lower $fn for preview mode
$fn = $preview ? floor(FN/4) : FN;
//**********************************************************************

//**********************************************************************
//* Parameters Sanity Check
//**********************************************************************
// Floor
assert(!(M_Floor_Type == "L" && M_Bin_Floor_Thickness >= GF_Height_U), str("Floor too thick for Lite Floor, must be less than:", GF_Height_U));
assert(!(M_Floor_Type == "B" && M_Bin_Floor_Thickness <= GF_Height_U), str("Floor too thin for Bin Fill Floor, must be greater than:", GF_Height_U));
assert(!(M_Floor_Type == "B" && M_Bin_Floor_Thickness > (M_Unit_Height+1) * GF_Height_U), str("Floor too thick for Bin Fill Floor"));

assert(M_Unit_Width >= 1 && floor(M_Unit_Width) == M_Unit_Width, "Width in units must be a positive integer");
assert(M_Unit_Depth >= 1 && floor(M_Unit_Depth) == M_Unit_Depth, "Depth in units must be a positive integer");
assert(DT_Clearance>=0, "DT_Clearance must be positive");
assert(GF_Base_Mounting_Base_Magnet_Pad>=0, "GF_Base_Mounting_Base_Magnet_Pad must be positive");
assert(GF_Base_Screw_Hole_Diameter>=0, "GF_Base_Screw_Hole_Diameter must be positive");
assert(GF_Base_Magnet_Diameter>=0, "GF_Base_Magnet_Diameter must be positive");
assert(GF_Base_Magnet_Hole_Depth>=0, "GF_Base_Magnet_Hole_Depth must be positive");
assert(GF_Base_Mounting_Total_Thickness>GF_Base_Magnet_Hole_Depth, "GF_Base_Mounting_Total_ThicknessNot thick enough");
assert(EPSILON>=0, "EPSILON must be positive");

//**********************************************************************
//* Bin Base - Core Grid
//*
// Return point list for bin base profile - Figure 2

// Limit check
assert(GF_Bin_Width_Side_Clearance + GF_Top_Wedge_Width + GF_Bin_Bottom_Wedge_Width + GF_Bin_Lower_Wedge_Minimum_Thickness <= GF_Corner_Radius, "GF_Bin_Lower_Wedge_Minimum_Thickness too large");
assert(GF_Bin_Lower_Wedge_Minimum_Thickness > 0, "GF_Bin_Lower_Wedge_Minimum_Thickness must be positive");

// The minimum Y coordinate of the flat floor
function bin_flat_floor_min_y() = GF_Bin_Bottom_Wedge_Width + GF_Top_To_Bottom_Wedge_Distance + GF_Top_Wedge_Width + GF_Bin_Width_Side_Clearance + GF_Bin_Base_Infill_Clearance + GF_Bin_Base_Infill_Thickness;
 
function bin_base_profile() =
[
/* 0 */  [ GF_Bin_Width_Side_Clearance + GF_Top_Wedge_Width + GF_Bin_Bottom_Wedge_Width, 0 ],
/* 1 */  [ GF_Bin_Width_Side_Clearance + GF_Top_Wedge_Width + GF_Bin_Bottom_Wedge_Width + GF_Bin_Lower_Wedge_Minimum_Thickness, 0 ],
/* 2 */  [ GF_Bin_Width_Side_Clearance + GF_Top_Wedge_Width + GF_Bin_Bottom_Wedge_Width + GF_Bin_Lower_Wedge_Minimum_Thickness,  GF_Height_U - GF_Bin_Side_Wall_Thickness - (GF_Corner_Radius - GF_Bin_Side_Wall_Thickness - GF_Bin_Width_Side_Clearance) ],
/* 3 */  [ GF_Bin_Width_Side_Clearance + GF_Bin_Side_Wall_Thickness, min(GF_Height_U - GF_Bin_Side_Wall_Thickness, bin_flat_floor_min_y())],
/* 4 */  [ GF_Bin_Width_Side_Clearance + GF_Bin_Side_Wall_Thickness,  bin_flat_floor_min_y()], // Shift down for a thinner floor
/* 5 */  [ GF_Bin_Width_Side_Clearance,  bin_flat_floor_min_y()],
/* 6 */  [ GF_Bin_Width_Side_Clearance, GF_Bin_Bottom_Wedge_Width + GF_Top_To_Bottom_Wedge_Distance + GF_Top_Wedge_Width ],
/* 7 */  [ GF_Bin_Width_Side_Clearance + GF_Top_Wedge_Width, GF_Bin_Bottom_Wedge_Width + GF_Top_To_Bottom_Wedge_Distance ],
/* 8 */  [ GF_Bin_Width_Side_Clearance + GF_Top_Wedge_Width, GF_Bin_Bottom_Wedge_Width ]
];

function infill_height() = GF_Bin_Base_Infill_Thickness;
function infill_base() = bin_base_profile()[6][1] + GF_Bin_Base_Infill_Clearance + GF_Bin_Width_Side_Clearance; // 

module Bin_Base_Grid_Unit()
{
  s_extrude_rectangle_rounded(GF_U_Width, GF_U_Width, GF_Corner_Radius, bin_base_profile());
}
//**********************************************************************

//**********************************************************************
//* Bin Base - Inter grid infill shapes to form a solid layer between grid units

// Inter Bin Base Side Join in X direction - a simple gap fill strip
module Bin_Base_Side_Join_X_Unit()
{
  translate(v=[GF_U_Width-GF_Bin_Width_Side_Clearance, GF_Corner_Radius, infill_base()]) 
    cube(size=[ 2*GF_Bin_Width_Side_Clearance + 2*EPSILON, 
                GF_U_Width - 2*GF_Corner_Radius, 
                infill_height()]);
}

// Inter Bin Base Side Join in Y direction - a simple gap fill strip
module Bin_Base_Side_Join_Y_Unit()
{
  translate(v=[GF_Corner_Radius, GF_U_Width-GF_Bin_Width_Side_Clearance, infill_base()]) 
    cube(size=[ GF_U_Width - 2*GF_Corner_Radius, 
                2*GF_Bin_Width_Side_Clearance + 2*EPSILON,
                infill_height()]);
}

// This is a clover shaped infill for the internal corners between 4 bin base profiles
// This is centered on the top right of unit 1 ready to grid replicate
module Bin_Base_Infill_Centre_Shape()
{
    difference() 
    { 
      // Main cuboid
      translate(v = [-GF_Corner_Radius, -GF_Corner_Radius, 0])
      {
        cube([2 * (GF_Corner_Radius + EPSILON), 
              2 * (GF_Corner_Radius + EPSILON), 
            infill_height()]);
      }

      // Chop out rounded corners
      s_linear_array(x_count=2, y_count=2, dx=2*GF_Corner_Radius, dy=2*GF_Corner_Radius) translate(v = [-GF_Corner_Radius, -GF_Corner_Radius , -EPSILON]) cylinder(h = infill_height() + 2*EPSILON, r = GF_Corner_Radius - GF_Bin_Width_Side_Clearance - EPSILON);
    }
}

// Shift to top right of lower left bin ready for grid replicaton
module Bin_Base_Infill_Centre()
{
  translate(v = [GF_U_Width, GF_U_Width, infill_base()]) Bin_Base_Infill_Centre_Shape();
}

// This is a clover shaped infill for the outer edges between a pair of bins
module Bin_Base_Infill_Edge_Shape()
{
  difference() 
  {
    Bin_Base_Infill_Centre_Shape();

    // Trim off the half plus a bit we don't want
    translate(v = [-GF_Corner_Radius - EPSILON, -GF_Corner_Radius - EPSILON, -EPSILON])  cube(size = [2 * GF_Corner_Radius + 4*EPSILON, GF_Corner_Radius + GF_Bin_Width_Side_Clearance + 2*EPSILON, infill_height() + 2*EPSILON]);
  }
}

// Position for each of the sides ready for array cloning

module Bin_Base_Infill_Edge_Bottom()
{
  translate(v = [GF_U_Width, 0, infill_base()]) Bin_Base_Infill_Edge_Shape();
}
module Bin_Base_Infill_Edge_Top()
{
   translate(v = [GF_U_Width, M_Unit_Depth*GF_U_Width, infill_base()]) rotate(a=180) Bin_Base_Infill_Edge_Shape();
}
module Bin_Base_Infill_Edge_Left()
{
   translate(v = [0, GF_U_Width, infill_base()]) 
   rotate(a=-90) Bin_Base_Infill_Edge_Shape();
}
module Bin_Base_Infill_Edge_Right()
{
   translate(v = [M_Unit_Width*GF_U_Width, GF_U_Width, infill_base()]) 
   rotate(a=90) Bin_Base_Infill_Edge_Shape();
}

module Bin_Base_Grid()
{
  group () 
  {
    // Generate Base Grid Core (Top) Section
    gf_grid_array() Bin_Base_Grid_Unit();

    // Join Bin Bases with infill strips
    if (M_Unit_Width > 1) gf_grid_array(x_count=M_Unit_Width-1, y_count=M_Unit_Depth) Bin_Base_Side_Join_X_Unit();
    if (M_Unit_Depth > 1) gf_grid_array(x_count=M_Unit_Width, y_count=M_Unit_Depth-1) Bin_Base_Side_Join_Y_Unit();

    // Fill in the internal corners
    if (M_Unit_Width > 1 && M_Unit_Depth > 1) gf_grid_array(x_count=M_Unit_Width-1, y_count=M_Unit_Depth-1) Bin_Base_Infill_Centre();
    // Fill in the side joins
    if (M_Unit_Width > 1) gf_x_array(x_count=M_Unit_Width-1) Bin_Base_Infill_Edge_Bottom();
    if (M_Unit_Width > 1) gf_x_array(x_count=M_Unit_Width-1) Bin_Base_Infill_Edge_Top();
    if (M_Unit_Depth > 1) gf_y_array(y_count=M_Unit_Depth-1) Bin_Base_Infill_Edge_Left();
    if (M_Unit_Depth > 1) gf_y_array(y_count=M_Unit_Depth-1) Bin_Base_Infill_Edge_Right();
  }
}

// Fig 2: 3,4,5,6 lifted to sit on 4,5 and height adjusted to M_Unit_Height * GF_Height_U high
function bin_base_sub_wall_profile() =
[
/* 0 */  [ GF_Bin_Width_Side_Clearance + GF_Bin_Side_Wall_Thickness,  bin_flat_floor_min_y() - EPSILON ],
/* 1 */  [ GF_Bin_Width_Side_Clearance,  bin_flat_floor_min_y() - EPSILON ],
/* 2 */  [ GF_Bin_Width_Side_Clearance,  GF_Height_U + EPSILON ],
/* 3 */  [ GF_Bin_Width_Side_Clearance + GF_Bin_Side_Wall_Thickness,  GF_Height_U + EPSILON ]
];
module Bin_Base_Sub_Wall() // This makes the bin up to 1 U high
{
  s_extrude_rectangle_rounded(width=GF_U_Width * M_Unit_Width, depth=GF_U_Width * M_Unit_Depth, corner_radius=GF_Corner_Radius, profile=bin_base_sub_wall_profile());
}
//**********************************************************************

//**********************************************************************
//* Bin Base - Floor Infill

// Lower base fill in - this will top out when thickness>floor_lower_base_max_thickness() as the next layer of infill will take over
module Bin_Base_Lower_Floor_Unit(thickness)
{
  floor_lower_base_max_thickness = (bin_base_profile()[2] - bin_base_profile()[1])[1];
  // Fig 2: Point 1.X
  base_edge = bin_base_profile()[1][0];
  if (thickness > 0)
  {
    floor_thickness = thickness > floor_lower_base_max_thickness ? floor_lower_base_max_thickness : thickness;
    translate(v = [base_edge - EPSILON, base_edge - EPSILON]) 
      cube( size=[GF_U_Width - 2 * ( base_edge - EPSILON ), 
        GF_U_Width - 2 * ( base_edge - EPSILON ), 
        floor_thickness]);
  }
}

// Mid base fill in - this will top out when thickness>floor_middle_base_max_thickness() as the next layer of infill will take over
module Bin_Base_Mid_Floor_Unit(thickness)
{
  floor_lower_base_max_thickness = dy_polygon_std(bin_base_profile(), 1, 2);
  floor_middle_base_max_thickness = dy_polygon_std(bin_base_profile(), 2, 3);

  profile_lower_x = bin_base_profile()[2][0];
  profile_upper_x = bin_base_profile()[3][0];
  if (thickness > floor_lower_base_max_thickness)
  {
    this_layer_thickness = thickness - floor_lower_base_max_thickness;
    floor_thickness = (this_layer_thickness) > floor_middle_base_max_thickness ? floor_middle_base_max_thickness : this_layer_thickness;
    translate(v = [GF_Corner_Radius-EPSILON, GF_Corner_Radius-EPSILON, floor_lower_base_max_thickness - EPSILON]) 
      hull()
      {
        s_linear_array(x_count=2, y_count=2, dx=GF_U_Width - 2*GF_Corner_Radius + 2*EPSILON, dy=GF_U_Width - 2*GF_Corner_Radius + 2*EPSILON) 
          cylinder(h = floor_thickness, r1 = 0.4, r2=floor_thickness);
      }
  }
}

// Upper base fill in - this will top out when thickness>floor_middle_base_max_thickness() as the next layer of infill will take over
module Bin_Base_Upper_Floor_Unit(thickness)
{
  floor_lower_base_max_thickness = dy_polygon_std(bin_base_profile(), 1, 2);
  floor_middle_base_max_thickness = dy_polygon_std(bin_base_profile(), 2, 3);
  floor_upper_base_max_thickness = dy_polygon_std(bin_base_profile(), 3, 4);

  this_layer_thickness = thickness - floor_lower_base_max_thickness - floor_middle_base_max_thickness;
  if (this_layer_thickness > 0)
  {
    floor_thickness = (this_layer_thickness) > floor_upper_base_max_thickness ? floor_upper_base_max_thickness : this_layer_thickness;
    translate(v = 
      [
        GF_Bin_Side_Wall_Thickness + GF_Bin_Width_Side_Clearance + EPSILON, 
        GF_Bin_Side_Wall_Thickness + GF_Bin_Width_Side_Clearance + EPSILON,
        floor_lower_base_max_thickness + floor_middle_base_max_thickness - EPSILON
        ]) 
      s_cube( size=[ GF_U_Width - 2 * (GF_Bin_Side_Wall_Thickness + GF_Bin_Width_Side_Clearance + EPSILON), 
                     GF_U_Width - 2 * (GF_Bin_Side_Wall_Thickness + GF_Bin_Width_Side_Clearance + EPSILON), 
                     floor_thickness], 
          corner_radius=GF_Corner_Radius - (GF_Bin_Side_Wall_Thickness + GF_Bin_Width_Side_Clearance) + EPSILON
      );
  }
}

// Bin main fill in
module Bin_Main_Floor(thickness)
{
  // This goes from 1 U height upwards
  this_layer_thickness = thickness - bin_flat_floor_min_y();
  if (this_layer_thickness > 0)
  {
    echo("Floor thickness = ", this_layer_thickness);
    translate(v=[GF_Bin_Side_Wall_Thickness + GF_Bin_Width_Side_Clearance - EPSILON, GF_Bin_Side_Wall_Thickness + GF_Bin_Width_Side_Clearance - EPSILON, bin_flat_floor_min_y()]) 
        s_cube( size=[ GF_U_Width * M_Unit_Width - 2 * (GF_Bin_Side_Wall_Thickness + GF_Bin_Width_Side_Clearance + EPSILON), 
                      GF_U_Width * M_Unit_Depth - 2 * (GF_Bin_Side_Wall_Thickness + GF_Bin_Width_Side_Clearance + EPSILON), 
                      this_layer_thickness], 
                corner_radius=GF_Corner_Radius - (GF_Bin_Side_Wall_Thickness + GF_Bin_Width_Side_Clearance) + EPSILON
        );
  }
}

module Bin_Base_Floor_Unit(thickness)
{
  Bin_Base_Lower_Floor_Unit(thickness=thickness);
  Bin_Base_Mid_Floor_Unit(thickness=thickness);
  Bin_Base_Upper_Floor_Unit(thickness=thickness);
}

module Bin_Base_Floor(thickness)
{
  gf_grid_array() Bin_Base_Floor_Unit(thickness=thickness);
  #Bin_Main_Floor(thickness=thickness);
}
//**********************************************************************

//**********************************************************************
//* Bin Wall - normally an integral multiplier of GF_U_Heigh but fractional will be allowed

// Fig 2: 3,4,5,6 lifted to sit on 4,5 and height adjusted to M_Unit_Height * GF_Height_U high
function bin_wall_profile() =
[
/* 0 */  [ GF_Bin_Width_Side_Clearance + GF_Bin_Side_Wall_Thickness,  GF_Height_U ],
/* 1 */  [ GF_Bin_Width_Side_Clearance,  GF_Height_U ],
/* 2 */  [ GF_Bin_Width_Side_Clearance,  GF_Height_U  * (M_Unit_Height + 1) ],
/* 3 */  [ GF_Bin_Width_Side_Clearance + GF_Bin_Side_Wall_Thickness,  GF_Height_U * (M_Unit_Height + 1) ]
];

module Bin_Wall()
{
  Bin_Base_Sub_Wall();
  s_extrude_rectangle_rounded(width=GF_U_Width * M_Unit_Width, depth=GF_U_Width * M_Unit_Depth, corner_radius=GF_Corner_Radius, profile=bin_wall_profile());
}
//**********************************************************************

//**********************************************************************
//* Bin Stacking Lip
//* The Y (which become Z after extrusion) will sit Y=0 on top of the bin all (usually a U Height multiple)

// Profile Y origin on Y:3 + GF_Stacking_Compensation
function bin_stacking_lip_profile() =
[
/* 0 */   [ 
            GF_Bin_Width_Side_Clearance, 
            - ( GF_Bin_Stacking_Lip_Top_Wedge_Width + GF_Bottom_Wedge_Height + GF_Bin_Stacking_Lip_Vert_Height ) + GF_Stacking_Compensation
          ],
/* 1 */   [ 
            GF_Bin_Width_Side_Clearance + GF_Bin_Stacking_Lip_Top_Wedge_Width + GF_Bottom_Wedge_Height,
            - GF_Bin_Stacking_Lip_Vert_Height + GF_Stacking_Compensation
          ],
/* 2 */   [ 
            GF_Bin_Width_Side_Clearance + GF_Bin_Stacking_Lip_Top_Wedge_Width + GF_Bottom_Wedge_Height, 
            GF_Stacking_Compensation 
          ],
/* 3 */   [ 
            GF_Bin_Width_Side_Clearance + GF_Bin_Stacking_Lip_Top_Wedge_Width,    
            GF_Bottom_Wedge_Height + GF_Stacking_Compensation
          ],
/* 4 */   [ 
            GF_Bin_Width_Side_Clearance + GF_Bin_Stacking_Lip_Top_Wedge_Width,  
            GF_Bottom_Wedge_Height + GF_Top_To_Bottom_Wedge_Distance + GF_Stacking_Compensation
          ],
/* 5 */   [ 
            GF_Bin_Width_Side_Clearance + GF_Top_Shoulder_Width,  
            GF_Bottom_Wedge_Height + GF_Top_To_Bottom_Wedge_Distance + GF_Bin_Stacking_Lip_Top_Wedge_Width - GF_Top_Shoulder_Width + GF_Stacking_Compensation
          ],
/* 6 */   [ 
            GF_Bin_Width_Side_Clearance,  
            GF_Bottom_Wedge_Height + GF_Top_To_Bottom_Wedge_Distance + GF_Bin_Stacking_Lip_Top_Wedge_Width - GF_Top_Shoulder_Width + GF_Stacking_Compensation
          ],

];
module Bin_Stacking_Lip()
{
  translate(v = [0, 0, GF_Height_U * (M_Unit_Height + 1) ] ) s_extrude_rectangle_rounded(GF_U_Width * M_Unit_Width, GF_U_Width * M_Unit_Depth, GF_Corner_Radius, bin_stacking_lip_profile());
}
//**********************************************************************

//**********************************************************************
//* Bin Stacking Lip
//* The Y (which become Z after extrusion) will sit Y=0 on top of the bin all (usually a U Height multiple)

// Profile Y origin on Y:3 + GF_Stacking_Compensation
function bin_stacking_lip_lite_profile() =
[
/* 0 */   [ 
            GF_Bin_Width_Side_Clearance, 
            GF_Bottom_Wedge_Height - 2 * GF_Bin_Stacking_Lip_Top_Wedge_Width + GF_Stacking_Compensation
          ],
/* 1 */   [ 
            GF_Bin_Width_Side_Clearance + GF_Bin_Stacking_Lip_Top_Wedge_Width,    
            GF_Bottom_Wedge_Height + GF_Stacking_Compensation
          ],
/* 2 */   [ 
            GF_Bin_Width_Side_Clearance + GF_Bin_Stacking_Lip_Top_Wedge_Width,  
            GF_Bottom_Wedge_Height + GF_Top_To_Bottom_Wedge_Distance + GF_Stacking_Compensation
          ],
/* 3 */   [ 
            GF_Bin_Width_Side_Clearance + GF_Top_Shoulder_Width,  
            GF_Bottom_Wedge_Height + GF_Top_To_Bottom_Wedge_Distance + GF_Bin_Stacking_Lip_Top_Wedge_Width - GF_Top_Shoulder_Width + GF_Stacking_Compensation
          ],
/* 4 */   [ 
            GF_Bin_Width_Side_Clearance,  
            GF_Bottom_Wedge_Height + GF_Top_To_Bottom_Wedge_Distance + GF_Bin_Stacking_Lip_Top_Wedge_Width - GF_Top_Shoulder_Width + GF_Stacking_Compensation
          ],

];
module Bin_Stacking_Lip_Lite()
{
  translate(v = [0, 0, GF_Height_U * (M_Unit_Height + 1) ] ) s_extrude_rectangle_rounded(GF_U_Width * M_Unit_Width, GF_U_Width * M_Unit_Depth, GF_Corner_Radius, bin_stacking_lip_lite_profile());
}
//**********************************************************************

//**********************************************************************
//* Fillets

// When we print a Lite bin with a recessed flloor, there is a 90 degree corner between
// wall and floor that's likely to be annoying when crabbing small parts - so this is a fillet
module Bin_Base_Lower_Floor_Fillet_Unit()
{
  floor_lower_base_max_thickness = dy_polygon_std(bin_base_profile(), 1, 2); // This will be the fillet height
  base_edge = bin_base_profile()[1][0]; // Fillets between these edges, inset from standard unit width
  fillet_length = GF_U_Width - 2 * base_edge;

  fillet_profile = [
    [0, 0],
    [floor_lower_base_max_thickness, 0],
    [0, floor_lower_base_max_thickness]
  ];
  translate([base_edge, base_edge, 0]) s_extrude_rectangle(width=fillet_length, depth=fillet_length, profile=fillet_profile);
}
//**********************************************************************

//**********************************************************************
//* Bin internal feature functions
// Return coordinates of inside of wall at unit width, depth
// This is useful for placing internal features so that they touch the wall at a specific corner
// This gives the bottom left coordinate, unite_width=0, unit_depth=0 would be the bin unit nearest the origin
function bin_wall_coordinates_bottom_left(unit_width = 0, unit_depth= 0) =
  [ 
    GF_U_Width * unit_width + GF_Bin_Width_Side_Clearance + GF_Bin_Side_Wall_Thickness,
    GF_U_Width * unit_depth + GF_Bin_Width_Side_Clearance + GF_Bin_Side_Wall_Thickness
  ];

// This gives the top right coordinate, unite_width=0, unit_depth=0 would be the bin unit nearest the origin
function bin_wall_coordinates_top_right(unit_width = M_Unit_Width-1, unit_depth= M_Unit_Depth-1) =
  [ 
    GF_U_Width * (unit_width + 1) - (GF_Bin_Width_Side_Clearance + GF_Bin_Side_Wall_Thickness),
    GF_U_Width * (unit_depth + 1) - (GF_Bin_Width_Side_Clearance + GF_Bin_Side_Wall_Thickness),
  ];
//**********************************************************************

//**********************************************************************
// Cookie Cutter - some features tend to punch through the rounded corners of the bin
// So we will construct a negative of the bin plan profile which can be used to trim
// any such features
// This will only effective trim from the bin top to the bin standard floor

module _Bin_Cookie_Cutter()
{
  // This needs to be as big as the bin's rectangular footprint but "too big" is fine
  bin_width = GF_U_Width * M_Unit_Width - 2 * GF_Bin_Width_Side_Clearance;
  bin_depth = GF_U_Width * M_Unit_Depth - 2 * GF_Bin_Width_Side_Clearance;
  bin_height = GF_Height_U * (M_Unit_Height + 2); // extra +1 includes stacking lib which can never be more than 1 U high
  cyl_r = GF_Corner_Radius - GF_Bin_Width_Side_Clearance;
  cutter_thickness = 10/100; // 10% over sized

  difference()
  {
    // Cookie Cutter outside
    translate(v = [-bin_width * cutter_thickness/2, -bin_depth * cutter_thickness/2, -bin_height * cutter_thickness/2]) 
      cube(size=[bin_width * (1+cutter_thickness), bin_depth * (1 + cutter_thickness), bin_height * (1 + cutter_thickness)]);

    // Cutout part: Easiest to make this from the hull of 4 cylinders
    hull()
    {
      translate(v = [ GF_Corner_Radius, GF_Corner_Radius]) cylinder(h = bin_height, r = cyl_r);
      translate(v = [ GF_U_Width * M_Unit_Width - GF_Corner_Radius, GF_Corner_Radius]) cylinder(h = bin_height, r = cyl_r);
      translate(v = [ GF_Corner_Radius, GF_U_Width * M_Unit_Depth - GF_Corner_Radius]) cylinder(h = bin_height, r = cyl_r);
      translate(v = [ GF_U_Width * M_Unit_Width - GF_Corner_Radius, GF_U_Width * M_Unit_Depth - GF_Corner_Radius]) cylinder(h = bin_height, r = cyl_r);
    }
  }
}

module Trim_Bin_Exterior()
{
  difference()
  {
    children(0);
    _Bin_Cookie_Cutter();
  }
}

//**********************************************************************

//**********************************************************************
//* Label ledge - optional triangular extrusion

module Label_Ledge() 
{
  // The top of this will be a small clearance below the origin so that the top can be translated to the bin (not stacking lip)
  // and not interfere with the bin above.
  // We will also round this off with a 1/2mm radius sphere
  rounding = 1;
  // We don't really want rounding on the back edges, so we'll make it deeper and let the Trim_Bin_Exterior() deal with the excess
  label_profile = [
    [-rounding, -M_Label_Top_Clearance - rounding],
    [M_Label_Depth - 2*rounding, -M_Label_Top_Clearance - rounding],
    [-rounding, -M_Label_Depth / tan(M_Label_Ledge_Overgang_Angle) - M_Label_Top_Clearance]
  ];

    // Max ledge length
    max_length = M_Unit_Width * GF_U_Width; // Cookie cutter will trim off excess

    length = M_Label == "F" ? max_length : M_Label_Width;

    // Translation options to make the if statement look more readable:
    left_pos = 0; // Translate x=0 for M_Label=="L"
    right_pos = max_length - length;
    centre_pos = (right_pos - left_pos) / 2;

    // Offset from furthest left it can start (which is GF_Corner_Radius)
    x_label_pos = M_Label=="L" ? left_pos :
        M_Label=="R" ? right_pos :
          centre_pos;
    Trim_Bin_Exterior()
    { 
      translate(v = [ x_label_pos,
                      bin_wall_coordinates_top_right(0, M_Unit_Depth - 1)[1] + EPSILON, 
                      GF_Height_U * (M_Unit_Height+1)
                    ]) 
        rotate(a=-90, v=[0,0,1]) mirror([0,0,1]) rotate(a=-90, v=[1,0,0]) 
          minkowski()
          {
            linear_extrude(height = length ) polygon(label_profile);
            sphere(r = rounding);
          }
    }
}
//**********************************************************************

//**********************************************************************
// Finger Scoop along front

module Scoop()
{
  // cube with a cylinder subtracted
  length = M_Unit_Width * GF_U_Width;
  Trim_Bin_Exterior()
  {
    translate(v = [0, bin_wall_coordinates_bottom_left()[1], M_Bin_Floor_Thickness])
    {
      difference() 
      {
        cube(size=[length, M_Scoop_Radius, M_Scoop_Radius]);
        translate(v = [0, M_Scoop_Radius, M_Scoop_Radius]) rotate(a = 90, v = [0,1,0]) cylinder(h = length, r = M_Scoop_Radius);
      }
    }
  }
}
//**********************************************************************

//**********************************************************************
// Rounded top edge cuboid - may be used as dividers, standoffs etc
// Origin is center X,Y and flat base for Z.
// It is assumed that you will use Trim_Bin_Exterior() after fitting.

module Cube_Rounded_Top(width, depth, height)
{
  edge_radius = 0.5;

  assert(width >= 2*edge_radius, str("Cube_Rounded_Top() width must be >= ", 2*edge_radius));
  assert(depth >= 2*edge_radius, str("Cube_Rounded_Top() depth must be >= ", 2*edge_radius));
  assert(height >= 2*edge_radius, str("Cube_Rounded_Top() height must be >= ", 2*edge_radius));

  difference() 
  {
    minkowski() 
    {
      translate ([0, 0, height / 2 - edge_radius]) cube(size = [width - 2 * edge_radius + EPSILON, depth - 2 * edge_radius + EPSILON, height + EPSILON], center = true);
      sphere(r = edge_radius);
    }
    translate(v = [0, 0, - 2* edge_radius]) cube(size = [width + 4 * edge_radius, depth + 4 * edge_radius, 4 * edge_radius], center=true);
  }
}

//**********************************************************************
//* Single divider for manual placement - aligns with floor, X,Y centered on origin, Y direction
// lenght, height and thickness are mm
module Divider_Single_Y(length=10, height=10, thickness=2) 
{
        translate([0, 0, bin_flat_floor_min_y()]) 
        Cube_Rounded_Top(thickness, length, height);
}
//**********************************************************************

//**********************************************************************
//* Generalised dividers
// Super flexible dividers which can be used to divide and even subdivide bins
module Dividers(divider_direction=0, dividers, gridstart=[1, 1], gridend=[M_Unit_Width, M_Unit_Depth], plength=100, pheight=100, thickness=M_Dividers_Thickness)
{
  // Divider direction is 0 for X, 1 for Y.
  repeat_direction = 1 - divider_direction;

  wall_offset = 2 * (GF_Bin_Width_Side_Clearance);

  repeat_unit_start = gridstart[divider_direction];
  repeat_unit_end = gridend[divider_direction];
  repeat_units = repeat_unit_end - repeat_unit_start + 1;
  
  length_unit_start = gridstart[repeat_direction];
  length_unit_end = gridend[repeat_direction];
  length_units = length_unit_end - length_unit_start + 1;

  divider_spacing = (GF_U_Width * repeat_units - wall_offset) / (dividers + 1);
  divider_length = (GF_U_Width * length_units - 2*wall_offset) * plength/100 + 2*wall_offset;
  divider_length_center = GF_U_Width * (length_unit_start-1 + length_unit_end)/2 - wall_offset;
  echo (divider_length_center);

  // FIXME - Magic number
  divider_height = (GF_Height_U * M_Unit_Height) * pheight/100 - 1;
  
  if (divider_direction == 0)
  {
    Trim_Bin_Exterior()
      gf_x_array(x_count=dividers, dx=divider_spacing) 
        translate([(GF_U_Width * (repeat_unit_start - 1)) + divider_spacing, divider_length_center + wall_offset/2, bin_flat_floor_min_y()]) 
        Cube_Rounded_Top(thickness, divider_length, divider_height);
  }
  else   
  {
    Trim_Bin_Exterior()
      gf_y_array(y_count=dividers, dy=divider_spacing) 
        translate([divider_length_center + wall_offset/2, (GF_U_Width * (repeat_unit_start - 1)) + divider_spacing, bin_flat_floor_min_y()]) 
        rotate(a=90) Cube_Rounded_Top(thickness, divider_length, divider_height);
  };
}
//**********************************************************************
