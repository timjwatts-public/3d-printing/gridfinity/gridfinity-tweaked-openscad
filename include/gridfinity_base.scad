//**********************************************************************
//* Core Base Modules and Functions

//* Dependencies on Global Variables:
//* Stardard variables from gridfinity-core-parameters.scad
//*
//* GF_Top_Wedge_Width
//* GF_Bottom_Wedge_Width
//* GF_Bottom_Wedge_Height
//* GF_Top_To_Bottom_Wedge_Distance
//* GF_Base_Padding
//* GF_U_Width
//* GF_Corner_Radius

//**********************************************************************
//* Base Top Grid
//*
// Return point list for base main profile
function base_main_profile(extra_base = 0) =
  [
    [ 0, 0 ],                                                                                                         // 0
    [ GF_Top_Wedge_Width + GF_Bottom_Wedge_Width, 0 ],                                                                // 1
    [ GF_Top_Wedge_Width + GF_Bottom_Wedge_Width, GF_Base_Padding + extra_base ],                                     // 2
    [ GF_Top_Wedge_Width, GF_Bottom_Wedge_Height + GF_Base_Padding + extra_base ],                                    // 3
    [ GF_Top_Wedge_Width, GF_Bottom_Wedge_Height + GF_Top_To_Bottom_Wedge_Distance + GF_Base_Padding + extra_base ],  // 4
    [
      GF_Top_Shoulder_Width,
      GF_Bottom_Wedge_Height + GF_Top_To_Bottom_Wedge_Distance + GF_Top_Wedge_Height + GF_Base_Padding +
      extra_base
    ],                                                                                                                // 5
    [
      0, GF_Bottom_Wedge_Height + GF_Top_To_Bottom_Wedge_Distance + GF_Top_Wedge_Height + GF_Base_Padding +
      extra_base
    ]                                                                                                                // 6
  ];

module Base_Core_Top_Grid_Unit(extra_base = 0)
{
  s_extrude_rectangle_rounded(width=GF_U_Width, depth=GF_U_Width, corner_radius=GF_Corner_Radius, profile=base_main_profile(extra_base=extra_base));
}
//**********************************************************************

//**********************************************************************
//* Optional Mounting Plate for Magnets and Screws
//*
// Single corner plate, offset from origin ready for mirroring
module Base_Mounting_Plate_Corner()
{
  // Shift 1/2 bin width down and left ready for mirroring
  translate([-GF_U_Width/2, -GF_U_Width/2, 0])
  {
    difference()
    {
      // Additive parts
      s_cube(size = [GF_Base_Mounting_Width, GF_Base_Mounting_Width, GF_Base_Mounting_Total_Thickness], corner_radius=GF_Corner_Radius);

      group() // Subtractive parts
      {
        // Magnet hole
        translate ([GF_Base_Hole_Centre_From_Corner, GF_Base_Hole_Centre_From_Corner, GF_Base_Mounting_Total_Thickness - GF_Base_Magnet_Hole_Depth]) cylinder(h = GF_Base_Magnet_Hole_Depth+EPSILON, r = GF_Base_Magnet_Diameter / 2);
        // Screw hole through
        translate ([GF_Base_Hole_Centre_From_Corner, GF_Base_Hole_Centre_From_Corner, -EPSILON]) cylinder(h = GF_Base_Mounting_Total_Thickness + 2*EPSILON, r = GF_Base_Screw_Hole_Diameter / 2);
      }

    }
  }
}
// Unit mounting plate for screw holes and magnets
module Base_Mounting_Plate_Unit()
{
  translate([ 42 / 2, 42 / 2, 0 ]) s_mirror_quad() Base_Mounting_Plate_Corner();
}
//**********************************************************************
