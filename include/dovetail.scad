//**********************************************************************
//* Dovetail generators
//**********************************************************************

//*********************************
//* Global variables
//
// EPSILON - Slight overlap to ensure no ambiguoius face joints
//*********************************

//*********************************
//* Parameters
//
//  clearance: gap between the tab and socket. 0.05mm works well for a firm friction fit on my Prusa Mk4 in PLA.
//    Adjust as required. 
//    The cutout is bigger all round by half this clearance. 
//    The tab is smaller all round by half this clearance
//  length: length side of the tab in mm, parallel to body being joined
//  width: how far the tab sticks out from the body being joined in mm
//  thickness: thickness of the dovetail from top to base in mm
//  angle: dovetail angle in degrees
//*********************************

// Generate a dovetail cutout shape which can be subtracted from an existing solid

module dovetailSlot(length, depth, thickness, angle, clearance, top_clearance) {
  dovetail_slot_points = [
    [-length/2 + depth/tan(angle) - clearance/2, -EPSILON],   // Rectangular extrusion to blend into the attached body
    [-length/2 + depth/tan(angle) - clearance/2, 0],
    [-length/2 - clearance/2, depth + clearance/2],
    [length/2 + clearance/2,  depth + clearance/2],
    [length/2 - depth/tan(angle) + clearance/2, 0],
    [length/2 - depth/tan(angle) + clearance/2, -EPSILON],    // Rectangular extrusion to blend into the attached body
  ];

  translate(v = [0,0,-EPSILON])                               // Slight shift down to ensure clean cut
  { 
    union() 
    {
      // Main dovetail socket
      linear_extrude(height=thickness+top_clearance) polygon(points = dovetail_slot_points);
      // Add triangular overhang to aid printing
      translate(v = [0,0,thickness + top_clearance]) linear_extrude(height=thickness/2, scale=0) polygon(points = dovetail_slot_points);
    }
  }
}

// Generate a dovetail tab which can be added to an existing solid

module dovetailTab(length, depth, thickness, angle, clearance)
{
  dovetail_tab_points = [
    [-length/2 + depth/tan(angle) + clearance/2, -EPSILON],  // Rectangular extrusion to blend into the attached body
    [-length/2 + depth/tan(angle) + clearance/2, 0],
    [-length/2 + clearance/2, depth - clearance/2],
    [length/2 - clearance/2,  depth - clearance/2],
    [length/2 - depth/tan(angle) - clearance/2, 0],
    [length/2 - depth/tan(angle) - clearance/2, -EPSILON],   // Rectangular extrusion to blend into the attached body
  ];

  linear_extrude(height=thickness) polygon(points = dovetail_tab_points);
}

