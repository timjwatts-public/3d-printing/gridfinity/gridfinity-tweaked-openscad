include <stdcore.scad>
//**********************************************************************
//* Helper Modules -  For brevity
//* Generate a linear array for the children parts with standard spacing to the number specified in the parameters
//*
//* Dependencies on Global Variables: 
//* GF_U_Width - Gridfinity Unit Width in mm
//* M_Unit_Width - Assembly width in standard Gridfinity Units GF_U_Width
//* M_Unit_Depth - Assembly depth in standard Gridfinity Units GF_U_Width
//*
// Generate a grid of children() M_Unit_Width wide and M_Unit_Depth deep
module gf_grid_array(x_count=M_Unit_Width, y_count=M_Unit_Depth, dx=GF_U_Width, dy=GF_U_Width)
{
  s_linear_array(x_count=x_count, y_count=y_count, dx=dx, dy=dy) children();
}
// Generate an array of children() M_Unit_Width wide in the x direction
module gf_x_array(x_count=M_Unit_Width, dx=GF_U_Width)
{
  s_linear_array(x_count=x_count, dx=dx) children();
}
// Generate an array of children() M_Unit_Depth deep in the y direction
module gf_y_array(y_count=M_Unit_Depth, dy=GF_U_Width)
{
  s_linear_array(y_count=y_count, dy=dy) children();
}
//**********************************************************************

//**********************************************************************
//* Optional Dovetails
//*
// Basic tab to save parameter repetition
module gf_dovetail_tab(length=DT_Length, depth=DT_Depth, thickness=DT_Thickness, angle=DT_Angle, clearance=DT_Clearance)
{
  dovetailTab(length=length, depth=depth, thickness=thickness, angle=angle, clearance=clearance);  
}
// Basic slot to save parameter repetition
module gf_dovetail_slot(length=DT_Length, depth=DT_Depth, thickness=DT_Thickness, angle=DT_Angle, clearance=DT_Clearance, top_clearance=DT_Socket_Top_Clearance)
{
  dovetailSlot(length=length, depth=depth, thickness=thickness, angle=angle, clearance=clearance, top_clearance=top_clearance); 
}

//**********************************************************************

// Generate lower left quadrant (-ve X, -ve Y) with origin shifted ready for reflective assembly
module _extrude_rectangle_rounded_quadrant(width, depth, corner_radius, profile)
{
  // Place in lower left quadrant
  translate([-width/2,-depth/2,0])
    // Make lower left quadrant of the shape
    union()
    {
      // 1/2 Left side
      translate ([0,corner_radius-EPSILON,0]) mirror([0,1,0]) rotate(90, [1,0,0]) linear_extrude(height = (depth - 2*corner_radius)/2 + 2*EPSILON) polygon(profile);
      //1/2 Bottom side
      translate ([corner_radius-EPSILON,0,0]) rotate (90, [0,0,1]) rotate(90, [1,0,0]) linear_extrude(height = ( width - 2*corner_radius)/2 + 2*EPSILON) polygon(profile );
      // Bottom left corner
      translate([corner_radius,corner_radius,0]) rotate_extrude(angle=90) translate([-corner_radius,0,0]) polygon(profile);
    }
}

// Assemble 4 of the above together using reflection
// Result will be a model centred on the origin
module s_extrude_rectangle_rounded(width, depth, corner_radius, profile, center=false)
{
  translate( center ? [0,0,0] : [width/2, depth/2, 0])
    s_mirror_quad() _extrude_rectangle_rounded_quadrant(width, depth, corner_radius, profile);

}