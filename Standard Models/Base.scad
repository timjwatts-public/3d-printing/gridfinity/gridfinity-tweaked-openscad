//**********************************************************************
//* Base Model Generator
//**********************************************************************
//#region Include Files
include <../include/stdcore.scad>
include <../include/dovetail.scad>
include <../include/gridfinity-core-parameters.scad>
//#endregion
//**********************************************************************
//* PARAMETERS
//**********************************************************************

//#region Parameters
/* [MODEL SIZE PARAMETERS] */
// Width of model in standard units
M_Unit_Width = 1;
// Depth of model in standard units
M_Unit_Depth = 1;

/* [DOVETAILS] */
// Top Dovetail Tab
DT_Top = true;
// Right Dovetail Tab
DT_Right = true;
// Bottom Dovetail Socket
DT_Bottom = true;
// Left Dovetail Socket
DT_Left = true;
// All round clearance so dovetail tab is a good fit in socket - This is one you may need to teak for your printer
DT_Clearance = 0.05;

/* [BASE PARAMETERS] */
// Add magnet/screw mounting pads
BASE_Mounting_Plate = false;
// Total thickness of base plate
GF_Base_Mounting_Total_Thickness = 3.9;
// How much material to add around the magnet cutout to the edge
GF_Base_Mounting_Base_Magnet_Pad = 2;
// Screw hole diameter
GF_Base_Screw_Hole_Diameter = 3;
// Magnet pocket diameter
GF_Base_Magnet_Diameter = 6.5;
// Magnet pocket depth
GF_Base_Magnet_Hole_Depth = 2.4;

/* [GRIDFINITY-TWEAKED TWEAKABLE PARAMETERS - Set to zero for "Official Gridfinity"] */
// COSMETIC: Zero is the original Gridfinity sharp top. Setting to a larger number will produced a flattened ledge of this M_Width on both the bins and the grid. Recommended to be a integer multiple of the printer nozzle size.
GF_Top_Shoulder_Width = 0.8;
// COMPENSATE: A little extra added to the bottom of the Grid base so bins seat fully. Recommend an integer multiple of the printer layer M_Depth.
GF_Base_Padding = 0.8;
// Facets to anything round
$fn = 100;
// EPSILON is a slight overlap fudge between components to ensure that they combine into a single solid correctly. Occasionally this may fail due to tiny floating point errors. Should be small enough to not be seen on the final part, bug enough to stop abberations.
EPSILON = 0.01;
//#endregion

//**********************************************************************
//* Derived Parameters
//**********************************************************************
GF_Bottom_Wedge_Width = GF_Bottom_Wedge_Height * tan(90 - GF_Bottom_Wedge_Angle);
GF_Top_Wedge_Height = (GF_Top_Wedge_Width - GF_Top_Shoulder_Width); // Assume angles are 45 degrees
GF_Base_Mounting_Width = GF_Base_Hole_Centre_From_Corner + GF_Base_Magnet_Diameter/2 + GF_Base_Mounting_Base_Magnet_Pad;

//**********************************************************************
//* Parameters Sanity Check
//**********************************************************************
assert(M_Unit_Width >= 1 && floor(M_Unit_Width) == M_Unit_Width, "Width in units must be a positive integer");
assert(M_Unit_Depth >= 1 && floor(M_Unit_Depth) == M_Unit_Depth, "Depth in units must be a positive integer");
assert(DT_Clearance>=0, "DT_Clearance must be positive");
assert(GF_Base_Mounting_Base_Magnet_Pad>=0, "GF_Base_Mounting_Base_Magnet_Pad must be positive");
assert(GF_Base_Screw_Hole_Diameter>=0, "GF_Base_Screw_Hole_Diameter must be positive");
assert(GF_Base_Magnet_Diameter>=0, "GF_Base_Magnet_Diameter must be positive");
assert(GF_Base_Magnet_Hole_Depth>=0, "GF_Base_Magnet_Hole_Depth must be positive");
assert(GF_Base_Mounting_Total_Thickness>GF_Base_Magnet_Hole_Depth, "GF_Base_Mounting_Total_ThicknessNot thick enough");
assert(EPSILON>=0, "EPSILON must be positive");

//**********************************************************************
//* Feature Modules - These assume global parameters set above so we do not waste
//*                   time adding arguments. These are to make the main assembly look readable.
//**********************************************************************

include <../include/gridfinity_utility.scad>
include <../include/gridfinity_base.scad>

//**********************************************************************
//* Main Model - this generates the STL
//* We try really hard to make customisable features be clear and additive as far
//* as possible
//*
difference()
{
  //*********************************************
  //* Everything here is added
  group()
  {
    // We need to add padding to the base to allow other base features to be included
    offset_for_mounting_pads = (BASE_Mounting_Plate ? GF_Base_Mounting_Total_Thickness : 0);

    // Generate Base Grid Core (Top) Section
    gf_grid_array() Base_Core_Top_Grid_Unit(extra_base = offset_for_mounting_pads);
    
    // Add in screw/magnet mounting plate if required
    if ( BASE_Mounting_Plate) gf_grid_array() Base_Mounting_Plate_Unit();

    // Add in optional dovetail tabs along top edge
    if (DT_Top) gf_x_array() translate([ GF_U_Width/2, GF_U_Width * M_Unit_Depth, 0 ]) gf_dovetail_tab();
    // Add in optional dovetail tabs along right edge
    if (DT_Right) gf_y_array() translate([ GF_U_Width * M_Unit_Width, GF_U_Width/2, 0 ]) rotate(-90) gf_dovetail_tab();

    //*
    //*********************************************
  }

  //*********************************************
  //* Everything here is subtracted
  group() 
  {
    // Trim out optional dovetail sockets along bottom edge
    if (DT_Bottom) gf_x_array() translate([ GF_U_Width/2, 0, 0 ]) gf_dovetail_slot();
    // Trim out optional dovetail sockets along left edge
    if (DT_Left) gf_y_array() translate([ 0, GF_U_Width/2, 0 ]) rotate(-90) gf_dovetail_slot();
  }
  //*
  //*********************************************
}
//**********************************************************************
