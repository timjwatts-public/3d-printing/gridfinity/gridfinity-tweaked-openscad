//**********************************************************************
//* Dovetailed side pads for base - to hold grid off the sides of a drawer
//**********************************************************************
//#region Include Files
include <../include/stdcore.scad>
include <../include/gridfinity-core-parameters.scad>
include <../include/dovetail.scad>
//#endregion
//**********************************************************************
//* PARAMETERS
//**********************************************************************

//#region Parameters
/* [MODEL PARAMETERS] */
// Length of pad strip in standard units
M_Unit_Length = 1;

// Length of pad strip in mm
P_Pad_Width = 5;

/* [Dovetails] */
// Dovetail Tab along top edge
DT_Tab = true;
// Dovetail Socket along bottom edge
DT_Socket = true;

/* [DOVETAIL PARAMETERS - these work - tweak at your own risk] */
// All round clearance so dovetail tab is a good fit in socket - This is one you may need to teak for your printer
DT_Clearance = 0.05;
// Dovetail long edge length
DT_Length = 4;
// Dovetail long edge to short edge distance
DT_Depth = 1.6;
// Dovetail tab thickness
DT_Thickness = 2;
// Dovetail angle
DT_Angle = 70;
// A little extra on the dovertail socket to ensure the tab seats fully
DT_Socket_Top_Clearance = 0.1;

/* [MODEL GENERATOR MICRO TWEAKS] */

// Facets to anything round
$fn = 100;
// EPSILON is a slight overlap fudge between components to ensure that they combine into a single solid correctly.
// Occasionally this may fail due to tiny floating point errors. Should be small enough to not be seen on the final
// part, bug enough to stop abberations.
EPSILON = 0.01;
//#endregion

//**********************************************************************
//* Helper Modules -  For brevity

// Generate a linear array along the x axis only for the children parts with standard spacing to the number specified in the parameters
module gf_x_array()
{
  s_linear_array(x_count = M_Unit_Length, dx=GF_U_Width) children();
}

//**********************************************************************

//**********************************************************************
//* Feature Modules - These assume the variables so we do not waste
//*                   time adding arguments. These are to make the
//*                   main assembly look readable.
//**********************************************************************

//**********************************************************************

//**********************************************************************
//* Optional Dovetails
//*
// Basic tab to save parameter repetition
module gf_dovetail_tab()
{
  dovetailTab(length = DT_Length, depth = DT_Depth, thickness = DT_Thickness, angle = DT_Angle, clearance = DT_Clearance);  
}
// Basic slot to save parameter repetition
module gf_dovetail_slot()
{
  dovetailSlot(length = DT_Length, depth = DT_Depth, thickness = DT_Thickness, angle = DT_Angle, clearance = DT_Clearance, top_clearance = DT_Socket_Top_Clearance); 
}
//**********************************************************************


//**********************************************************************
//* Main Model - this generates the STL
difference()
{
  //*********************************************
  //* Everything here is added
  group()
  {
    // Generate Base Grid Core (Top) Section
    cube([M_Unit_Length * GF_U_Width, P_Pad_Width, DT_Thickness]);

    // Add in optional dovetail tabs along top edge
    if (DT_Tab) gf_x_array() translate([ GF_U_Width/2, P_Pad_Width, 0 ]) gf_dovetail_tab();

    //*
    //*********************************************
  }

  //*********************************************
  //* Everything here is subtracted
  group() 
  {
    // Trim out optional dovetail sockets along bottom edge
    if (DT_Socket) gf_x_array() translate([ GF_U_Width/2, 0, 0 ]) gf_dovetail_slot();
  }
  //*
  //*********************************************
}
//**********************************************************************
