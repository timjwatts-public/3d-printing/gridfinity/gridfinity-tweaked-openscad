# Gridfinity Tweaked
## Licence
Creative Commons with Attribution 4.0
See: https://creativecommons.org/licenses/by/4.0/
Full text in LICENSE file included.

Author:
Tim Watts <tw_gitlab@dionic.net>
Copyright 2023

## This aims to produce Gridfinity grids and bins with these objectives: 
1. Generic Bins and Grids fully customisable;
1. Backwards compatible with Zack's Gridfinity;
1. Extra options: 
   - Dovetails on grid sections to any size grid may be built without screws;
   - *Lite* bin floor (fast and minimal plastic);
   - Any floor thickness from zero (skeleton) upwards *(bin fill not yet implemented)*;
   - Simplified backwards compatible stacking lip;
   - No stacking lip;
1. Minimal use of plastic - thin walls but customisable;
1. Simple to read main model file with clear **module per feature** additive and subtractive build.

## Using Standard Models
1. Install OpenSCAD from your OS package manager, or from the official website: https://openscad.org/downloads.html (version **2021.01** or later);
1. Download these models and supporting files and unzip into an empty directory;
1. Start OpenSCAD and open one of the files from: `<place-you-unpacked-the-zip-file>/Standard Models/`
1. Adjust the parameters in OpenSCAD's customiser. The most useful ones at first will be:
   1. M_Unit_Width
   1. M_Unit_Depth
   1. M_Unit_Height (Bins only)
   1. DT_Top, etc (Base only)
1. When done, click the Render Button (this might take a minute or three depending on your computer's processing power);
1. Click the Export As STL button, save the file, slice and print.

## Deviations from Zack's Official Gridfinity Specifications
- **Floor Lite** (M_Floor_Type="L"): does not infill the floor into the locating grid profiles. This is fine for single bins that hold loose items and larger boxes that will just get odd larg objects stored stuch as tape measures, pliars, pens etc. You can adjust the floor thickness. *Backwards compatible with Standard Gridfinity!*
- **Stacking Lip** (M_Stacking_Lip_Type [S:Standard Lip, L:Lite Lip, N:No Stacking lip]): Standard is Zack's. Lite loses the lower edge which is often not necesssary and leaves a less lumpy top lip. No Lip is as it says - useful for tall boxes that will never have anything stacked on top. *Lite and Standard are backwards compatible with Standard Gridfinity!*
- **Flattened top edge** GF_Top_Shoulder_Width: kcocks a little off an otherwise sharp top. Don't overdo this. *Backwards compatible with Standard Gridfinity (mostly)* 
- **GF_Stacking_Compensation**: Add a tiny bit extra to the stacking lip height to ensure different size bins all stack up to the same boundary heights. 0=Official Gridfinity, 0.1-0.3mm maye just help even heights depending on your printer. *Backwards compatible with Standard Gridfinity* 

## Coding notes
1. Grifinity core parameters are in `include/gridfinity-core-parameters.scad` and not expected to be changed - but could be, for non standard grid sizes. YMMV.
1. Model parameters are set in the main model files `Bin.scad`, `Base.scad` etc.
1. Whilst *usually considered poor practice*, functions and modules make use of global variables for default parameters. This makes the main model readable by assuming that component is to be constructed for a Bin Z high, X units wide, Y units deep etc. To pass everythign by paramter all the time just leads to 3 line long module calls.
1. Despite the above, we aim to allow parameters to be selectively overriden where something less common is needed.
1. By putting the nitty gritty for bins into `include/gridfinity_bin.scad` (etc) we aim that a standard customisable Bin.scad model may be copied and easily edited to build a really custom bin, eg with irregular dividers to locate tools, or add a row of hex bit holders.

# Outstanding issues and bugs
1. Bins do not have a base magnet option yet - I expect to add in the next release soon.
1. Probably many undetected issues - I have only tested on a Prusa Mk4 printer with 0.4 nozzle and decent quality PLA