# Gridfinity Tweaked - Release History

## Latest Version: v0.1.0
### Release date: 2023-11-29
### Features
- First official release
- Grid with optional dovetails complete
- Basic bins with option label ledge, finger scoop and regular X and Y compartment dividers.
### Known bugs or defects
- No magnet holes in bins yet. Expected in very near future.
- Only tested on Prusa Mk4 with decent quality PLA and 0.4mm nozzle. Your milage may vary. In particular, try increasing wall thickness if you have problems with cracking.
- A lot of plastic and print time savings can be made by reduncing the number of solid layers on the botton and top of solid sections. These are settings that can be (probably) found in your slicer. I find that 2 layers base and 3 layers top gives a decent finish with my setup. YMMV.