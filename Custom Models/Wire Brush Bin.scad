//**********************************************************************
//* Bin Model Generator
//**********************************************************************
//#region Include Files
include <../include/stdcore.scad>
include <../include/dovetail.scad>

include <../include/gridfinity-core-parameters.scad>
//#endregion
//**********************************************************************
//* PARAMETERS
//**********************************************************************

//#region Parameters
/* [MODEL SIZE PARAMETERS] */
// Width of model in standard 42mm units
M_Unit_Width = 1;
// Depth of model in standard 42mm units
M_Unit_Depth = 5;
// Height of model in standard 7mm units
M_Unit_Height = 7;

/* [BASE PARAMETERS] */
// Screw hole diameter
GF_Bin_Magnet_Diameter = 6.5;
// Magnet pocket depth
GF_Bin_Magnet_Hole_Depth = 2.4;
// Floor type
M_Floor_Type="T"; // [L:Lite Floor, F:Full Standard Floor, B:Bin Fill, T:Tweaked Minimum Flat Floor]
// Floor thickness
M_Desired_Floor_Thickness = 0.6;
// Fillet for Lite floors
M_Lite_Floor_Fillet = false;

/* [Features] */
// Stacking Lip
M_Stacking_Lip_Type="S"; // [S:Standard Lip, L:Lite Lip, N:No Stacking lip]
// Label Ledge: This will go at the rear of the bin
M_Label = "F"; // [N: None, F:Full Width, L:Left, C:Centre, R:Right]
// Label ledge distance front to rear - probably want to be 2mm wider than your label tape width
M_Label_Depth = 10;
// Width of Centre, Right and Left label ledge
M_Label_Width = 25;
// Gap above top of label ledge from base of stacked bin above
M_Label_Top_Clearance = 0.5;
// Label Ledge Angle - 45 degress is safe for most printers but may punch through the base for a 1U bin and very wide ledge. 60 degree may work for some printers with the right nozzel and settings
M_Label_Ledge_Overgang_Angle = 45;
// Finger scoop along front

/* [Hidden] */
M_Scoop = false;
// Finger scoop radius
M_Scoop_Radius = 5;

/* [Hidden] */
// Vertical dividers
M_Dividers_Vertical = 0;
// Percentage of available space
M_Dividers_Vertical_Length = 100;

// Vertical dividers
M_Dividers_Horizontal = 0;
// Percentage of available space
M_Dividers_Horizontal_Length = 100;

// Percentage of available height
M_Dividers_Height = 70;
/* [Dividers] */
M_Dividers_Thickness = 2.0;


/* [ Thickness] */
// Vertical side wall thickness - this should be OK for smaller bins. Probably want to increase a bit for large bins or they can fracture along layer joins.
GF_Bin_Side_Wall_Thickness = 1.2;
// Lower wedge thickness: Go thicker for a stronger base, but is limited by OpenSCAD rotate_extrude()
GF_Bin_Lower_Wedge_Minimum_Thickness = 0.4;

/* [GRIDFINITY-TWEAKED TWEAKABLE PARAMETERS - Set to zero for "Official Gridfinity"] */
// DEBUG - Sectional cut-away so you can see how the tweak parameters are affecting the cross section
DEBUG_Sectional_Cutaway = false;
// COSMETIC: Zero is the original Gridfinity sharp top. Setting to a larger number will produced a flattened ledge of this M_Width on both the bins and the grid. Recommended to be a integer multiple of the printer nozzle size.
GF_Top_Shoulder_Width = 0.8;
// COMPENSATE:  0 for true Gridfinity. Slight M_Depth pad on the top bin stacking lip to ensure that different M_Depth bins stack to the same integral U M_Depth. This compensates for the clearance between bins and grid which may see the bin sitting slightly low.
GF_Stacking_Compensation = 0.3;
// Clearance between highest point of grid base and the inter-bin infill (need clearance to account for filament sag)
GF_Bin_Base_Infill_Clearance = 0.2;
// Thickness of inter bin base infill fillet - this determines: a) The strength of linkage between units; b) The lowest a flat floor can be.
GF_Bin_Base_Infill_Thickness = 0.6;
// Vertical height under the profile, Fig 3: Y:1-2
GF_Bin_Stacking_Lip_Vert_Height = 0.4;
// Facets to anything round
FN = 80;
// EPSILON is a slight overlap fudge between components to ensure that they combine into a single solid correctly. Occasionally this may fail due to tiny floating point errors. Should be small enough to not be seen on the final part, bug enough to stop abberations.
EPSILON = 0.02;
//#endregion

//**********************************************************************
//* Feature Modules - These assume global parameters set above so we do not waste
//*                   time adding arguments. These are to make the main assembly look readable.
//**********************************************************************

include <../include/gridfinity_utility.scad>
include <../include/gridfinity_bin.scad>

//**********************************************************************
//* Main Model - this generates the STL
//* We try really hard to make customisable features be clear and additive as far
//* as possible
//*
difference()
{
  //******** Everything here is added
  group()
  {
    // Build the base frame and join them - this is basically "bin-lite"
    Bin_Base_Grid();

    // Add in the floor
    Bin_Base_Floor(thickness=M_Bin_Floor_Thickness);

    // Fillet
    if (M_Lite_Floor_Fillet && M_Floor_Type=="L") gf_grid_array() Bin_Base_Lower_Floor_Fillet_Unit();
    // Extra bin floor fill
    if (M_Floor_Type=="B") Bin_Main_Floor(thickness=M_Bin_Floor_Thickness);


    // Add in bin wall
    Bin_Wall();

    // Optional Stacking Lip
    if (M_Stacking_Lip_Type=="S") Bin_Stacking_Lip();
    if (M_Stacking_Lip_Type=="L") Bin_Stacking_Lip_Lite();
    
    // Optional Label Ledge
    if (M_Label != "N") Label_Ledge();

    // These features all require at least a standard floor or thicker. Otherwise we'd have to trim the feature bases to not cut through the base grid
    // So this means that the Lite bin floor is mostly likely best for single unit containers or containers that take items where having a 
    // regular floor matters less.
    if (M_Floor_Type != "L")
    {
      if (M_Scoop) Scoop();

      // Add X direction dividers if required
      if (M_Dividers_Vertical > 0) 
        Dividers(divider_direction=0, dividers=M_Dividers_Vertical, plength=M_Dividers_Vertical_Length, pheight=M_Dividers_Height, thickness=M_Dividers_Thickness);

      // Add Y direction dividers if required
      if (M_Dividers_Horizontal > 0) 
        Dividers(divider_direction=1, dividers=M_Dividers_Horizontal, plength=M_Dividers_Horizontal_Length, pheight=M_Dividers_Height, thickness=M_Dividers_Thickness);
      
      // Example of using repeating dividers manually to construct more arbirary shapes
      Dividers(divider_direction=0, dividers=1, pheight=65);
      Dividers(divider_direction=1, dividers=1, pheight=65, gridstart=[1,1], gridend=[1,3]);
    }
    
  }

  //******** Everything here is subtracted
  group() 
  {
    if (DEBUG_Sectional_Cutaway) cube([200,20, 50], center=true);
  }
}
//**********************************************************************